#!/bin/bash
# 
# TODO: Implement the logic for the commands below. 
# 
# Usage:
#  build - Create docker container for the backend application
#  run   - Run the backend as a container (you can use port 5000 in your port mapping)
#  stop  - Stop the backend container
if [ "$1" == "build" ]; then
        docker-compose build
    elif [ "$1" == "up" ]; then
        docker-compose up -d
    elif [ "$1" == "down" ]; then
        docker-compose down
    else
        echo "Usage: ./manage.sh build|up|down"
fi